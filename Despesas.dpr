program Despesas;

uses
  Vcl.Forms,
  classe.Spend in 'models\classe.Spend.pas',
  classe.Category in 'models\classe.Category.pas',
  classe.Period in 'models\classe.Period.pas',
  classe.MakeSpend in 'controller\classe.MakeSpend.pas',
  classe.MakeCategory in 'controller\classe.MakeCategory.pas',
  classe.MakePeriod in 'controller\classe.MakePeriod.pas',
  untDM in 'untDM.pas' {frmDM: TDataModule},
  untPrincipal in 'view\untPrincipal.pas' {frmPrincipal},
  untTable in 'view\untTable.pas' {frmTableVisualizer};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmDM, frmDM);
  Application.CreateForm(TfrmPrincipal, frmPrincipal);
  Application.CreateForm(TfrmTableVisualizer, frmTableVisualizer);
  Application.Run;
end.

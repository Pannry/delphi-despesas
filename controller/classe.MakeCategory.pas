unit classe.MakeCategory;

interface
type
  TMakeCategory = class
  private
    //
  public
    procedure createType(valName : String);
  end;
implementation

{ TMakeType }

uses classe.Category;

procedure TMakeCategory.createType(valName: String);
var
  MyType : TCategory;

begin
  MyType := TCategory.Create;

  try
    MyType.Name := valName;
    MyType.SaveInDatabase;

  finally
    MyType.Free;

  end;

end;

end.

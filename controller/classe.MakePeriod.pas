unit classe.MakePeriod;

interface
type
  TMakePeriod = class
  private
    //
  public
    procedure createPeriod(valMonth: String; valYear : Integer);
    procedure SearchPeriod(valSearch, valPeriod : String);
  end;

implementation

{ TMakePeriod }

uses classe.Period, System.sysUtils;

procedure TMakePeriod.createPeriod(valMonth : String; valYear: Integer);
var
  MyPeriod : TPeriod;
begin
  MyPeriod := TPeriod.Create;

  try
    MyPeriod.Month := valMonth;
    MyPeriod.Year := IntToStr(valYear);
    MyPeriod.SaveInDatabase;

  finally
    MyPeriod.Free;

  end;
end;

procedure TMakePeriod.SearchPeriod(valSearch, valPeriod: String);
var
  MyPeriod : TPeriod;
begin
  MyPeriod := TPeriod.Create;

  try
    MyPeriod.SearchPeriod(valSearch, valPeriod);

  finally
    MyPeriod.Free;

  end;
end;

end.

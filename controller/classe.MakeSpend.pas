unit classe.MakeSpend;

interface
uses classe.Spend, System.SysUtils;

type
  TMakeSpend = class
  private
    //
  public
    procedure createSpend(valName : String; valCurr : Currency; valDate : TDateTime);
  end;

implementation

{ TMakeSpend }

//  METHODS
procedure TMakeSpend.createSpend(valName: String; valCurr: Currency; valDate : TDateTime);
var
  MySpend : TSpend;

begin
  MySpend := TSpend.Create;

  try
    MySpend.Name := valName;
    MySpend.Spend := valCurr;
    MySpend.Date := valDate;
    MySpend.SaveInDatabase;

  finally
    MySpend.Free;

  end;

end;

end.

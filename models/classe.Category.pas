unit classe.Category;

interface

uses System.SysUtils;

type
  TCategory = class
  private
    FName : String;
    procedure SetName(const Value: String);

  public
    property Name : String read FName write SetName;
    procedure SaveInDatabase;
  end;

implementation

{ TTypeSpend }

uses untDM;

//*************************************
// METHODS
//*************************************
procedure TCategory.SaveInDatabase;
begin
  untDM.frmDM.qrCategory.Open;
  untDM.frmDM.qrCategory.Insert;
  untDM.frmDM.qrCategoryCATEGORY.AsString := Name;
  untDM.frmDM.qrCategory.Post;

end;

//*************************************
// GETTERS AND SETTERS
//*************************************
procedure TCategory.SetName(const Value: String);
begin
  FName := Value;
end;

end.

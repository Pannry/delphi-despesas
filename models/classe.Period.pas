unit classe.Period;

interface

uses System.SysUtils;

type
  TPeriod = class
  private
    { private declarations }
    FMonth : String;
    FYear : String;
    procedure SetMonth(const Value: String);
    procedure SetYear(const Value: String);

  public
    { public declarations }
    property Month : String read FMonth write SetMonth;
    property Year : String read FYear write SetYear;

    procedure SaveInDatabase;
    procedure SearchPeriod(valSearch, valPeriod: String);
  end;

implementation

{ TPeriod }

uses untDM;

procedure TPeriod.SaveInDatabase;
begin
  untDM.frmDM.qrCategory.Open;
  untDM.frmDM.qrPeriod.Insert;
  untDM.frmDM.qrPeriodMONTH.AsString := Month;
  untDM.frmDM.qrPeriodYEAR.AsString := Year;
  untDM.frmDM.qrPeriod.Post;
end;

procedure TPeriod.SearchPeriod(valSearch, valPeriod: String);
var
  Line : String;
begin
  untDM.frmDM.qrSearchPeriod.Close;

  Line := 'SELECT * FROM period WHERE period.":Search" like %:Time%';
  untDM.frmDM.qrSearchPeriod.SQL.Text := Line;
  untDM.frmDM.qrSearchPeriod.ParamByName('Search').Text := valSearch;
  untDM.frmDM.qrSearchPeriod.ParamByName('Time').Text := valPeriod;
  untDM.frmDM.qrSearchPeriod.Open;
end;

procedure TPeriod.SetMonth(const Value: String);
begin
  FMonth := Value;
end;

procedure TPeriod.SetYear(const Value: String);
begin
  FYear := Value;
end;

end.

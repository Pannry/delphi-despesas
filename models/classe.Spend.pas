unit classe.Spend;

interface

uses System.SysUtils;

type
  TSpend = class
  private
    FName : String;
    FSpend : Currency;
    FDate: TDateTime;
    procedure SetName(const Value: String);
    procedure SetSpend(const Value: Currency);
    procedure SetDate(const Value: TDateTime);

  public
    property Name : String read FName write SetName;
    property Spend : Currency read FSpend write SetSpend;
    property Date : TDateTime read FDate write SetDate;

    procedure SaveInDatabase;
  end;

implementation

{ TSpend }

//*************************************
// METHODS
//*************************************
procedure TSpend.SaveInDatabase;
var
  MyFile : TextFile;
  FileName : String;

begin
  FileName := 'Gasto.txt';
  AssignFile(myFile, FileName);

  if not FileExists(FileName) then
  begin
    ReWrite(myFile);
    WriteLn(myFile, 'Nome,Valor,Data');
  end;

  Append(myFile);
  WriteLn(myFile, Name + ',"' + CurrToStr(Spend) + '","' + DateTimeToStr(Date)+'"');
  CloseFile(myFile);
end;

//*************************************
// GETTERS AND SETTERS
//*************************************
procedure TSpend.SetDate(const Value: TDateTime);
begin
  FDate := Value;
end;

procedure TSpend.SetName(const Value: String);
begin
  FName := Value;
end;

procedure TSpend.SetSpend(const Value: Currency);
begin
  FSpend := Value;
end;

end.

object frmDM: TfrmDM
  OldCreateOrder = False
  Height = 377
  Width = 722
  object fdConn: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\Thales\Documents\Embarcadero\Studio\Projects\D' +
        'espesas\Database\SPEND.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'DriverID=FB')
    LoginPrompt = False
    Left = 104
    Top = 24
  end
  object qrCategory: TFDQuery
    Connection = fdConn
    SQL.Strings = (
      'SELECT * FROM CATEGORY')
    Left = 35
    Top = 121
    object qrCategoryID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      ReadOnly = True
      Visible = False
    end
    object qrCategoryCATEGORY: TStringField
      FieldName = 'CATEGORY'
      Origin = 'CATEGORY'
      Required = True
      Size = 30
    end
  end
  object qrPeriod: TFDQuery
    Connection = fdConn
    SQL.Strings = (
      'select * from period')
    Left = 152
    Top = 120
    object qrPeriodID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      ReadOnly = True
      Visible = False
    end
    object qrPeriodMONTH: TStringField
      DisplayLabel = 'M'#234's'
      FieldName = 'MONTH'
      Origin = '"MONTH"'
      Required = True
    end
    object qrPeriodYEAR: TStringField
      DisplayLabel = 'Ano'
      FieldName = 'YEAR'
      Origin = '"YEAR"'
      Required = True
      Size = 10
    end
  end
  object qrSearchPeriod: TFDQuery
    Connection = fdConn
    Left = 656
    Top = 16
  end
  object qrTableVisualizer: TFDQuery
    OnCalcFields = qrTableVisualizerCalcFields
    ConstraintsEnabled = True
    Connection = fdConn
    SQL.Strings = (
      
        'SELECT period."MONTH", period."YEAR", spend.name, spend.spend, s' +
        'pend."DATE", category.category'
      'FROM Period'
      'INNER JOIN period_spend ON period.id = period_spend.id_period'
      'INNER JOIN spend ON period_spend.id_spend = spend.id'
      'INNER JOIN category ON spend.id_category = category.id'
      'where period."MONTH" = :SelectedMonth;')
    Left = 656
    Top = 88
    ParamData = <
      item
        Name = 'SELECTEDMONTH'
        DataType = ftString
        ParamType = ptInput
        Size = 20
        Value = Null
      end>
    object qrTableVisualizerMONTH: TStringField
      DisplayLabel = 'M'#234's'
      DisplayWidth = 20
      FieldName = 'MONTH'
      Origin = '"MONTH"'
      Required = True
    end
    object qrTableVisualizerYEAR: TStringField
      DisplayLabel = 'Ano'
      DisplayWidth = 10
      FieldName = 'YEAR'
      Origin = '"YEAR"'
      Required = True
      Size = 10
    end
    object qrTableVisualizerNAME: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Nome'
      DisplayWidth = 40
      FieldName = 'NAME'
      Origin = 'NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 40
    end
    object qrTableVisualizerSPENDCALC: TStringField
      DisplayLabel = 'Gasto'
      DisplayWidth = 15
      FieldKind = fkCalculated
      FieldName = 'SPENDCALC'
      Size = 30
      Calculated = True
    end
    object qrTableVisualizerSPEND: TBCDField
      AutoGenerateValue = arDefault
      DisplayWidth = 19
      FieldName = 'SPEND'
      Origin = 'SPEND'
      ProviderFlags = []
      ReadOnly = True
      Visible = False
      Precision = 18
      Size = 2
    end
    object qrTableVisualizerDATE: TDateField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Data'
      DisplayWidth = 10
      FieldName = 'DATE'
      Origin = '"DATE"'
      ProviderFlags = []
      ReadOnly = True
    end
    object qrTableVisualizerCATEGORY: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Categoria'
      DisplayWidth = 25
      FieldName = 'CATEGORY'
      Origin = 'CATEGORY'
      ProviderFlags = []
      ReadOnly = True
      Size = 30
    end
  end
end

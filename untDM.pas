unit untDM;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  FireDAC.Stan.StorageBin, FireDAC.Phys.SQLiteVDataSet, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.SQLite, FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs,
  FireDAC.VCLUI.Wait, FireDAC.DApt, FireDAC.Phys.FB, FireDAC.Phys.FBDef;

type
  TfrmDM = class(TDataModule)
    fdConn: TFDConnection;
    qrCategory: TFDQuery;
    qrCategoryID: TIntegerField;
    qrCategoryCATEGORY: TStringField;
    qrPeriod: TFDQuery;
    qrPeriodID: TIntegerField;
    qrPeriodMONTH: TStringField;
    qrPeriodYEAR: TStringField;
    qrSearchPeriod: TFDQuery;
    qrTableVisualizer: TFDQuery;
    qrTableVisualizerMONTH: TStringField;
    qrTableVisualizerYEAR: TStringField;
    qrTableVisualizerNAME: TStringField;
    qrTableVisualizerSPEND: TBCDField;
    qrTableVisualizerDATE: TDateField;
    qrTableVisualizerCATEGORY: TStringField;
    qrTableVisualizerSPENDCALC: TStringField;
    procedure qrTableVisualizerCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDM: TfrmDM;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TfrmDM.qrTableVisualizerCalcFields(DataSet: TDataSet);
begin
  qrTableVisualizerSPENDCALC.AsString := CurrToStr(qrTableVisualizerSPEND.Value);
end;

end.

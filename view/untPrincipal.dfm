object frmPrincipal: TfrmPrincipal
  Left = 0
  Top = 0
  Caption = 'frmPrincipal'
  ClientHeight = 681
  ClientWidth = 1264
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 19
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 1264
    Height = 81
    Align = alTop
    TabOrder = 0
  end
  object Panel5: TPanel
    Left = 0
    Top = 153
    Width = 1264
    Height = 528
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object cpnActiveArea: TCardPanel
      Left = 0
      Top = 0
      Width = 1264
      Height = 528
      Align = alClient
      ActiveCard = cardSpend
      BevelOuter = bvNone
      TabOrder = 0
      object cardSpend: TCard
        Left = 0
        Top = 0
        Width = 1264
        Height = 528
        Caption = 'cardSpend'
        CardIndex = 0
        TabOrder = 0
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 1264
          Height = 528
          Align = alClient
          TabOrder = 0
          object Panel6: TPanel
            AlignWithMargins = True
            Left = 361
            Top = 11
            Width = 331
            Height = 506
            Margins.Left = 10
            Margins.Top = 10
            Margins.Right = 10
            Margins.Bottom = 10
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object btnInsertPeriod: TSpeedButton
              Left = 0
              Top = 0
              Width = 331
              Height = 56
              Align = alTop
              Caption = 'Inserir per'#237'odo'
              OnClick = btnInsertPeriodClick
              ExplicitLeft = -4
              ExplicitTop = -5
              ExplicitWidth = 397
            end
            object btnSearchMonth: TSpeedButton
              Left = 3
              Top = 72
              Width = 150
              Height = 30
              GroupIndex = 1
              Down = True
              Caption = 'Procurar por m'#234's'
              Flat = True
            end
            object btnSearchYear: TSpeedButton
              Left = 154
              Top = 72
              Width = 150
              Height = 30
              GroupIndex = 1
              Caption = 'Procurar por ano'
              Flat = True
            end
            object Panel8: TPanel
              AlignWithMargins = True
              Left = 3
              Top = 201
              Width = 325
              Height = 302
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 0
              object dbGridPeriodo: TDBGrid
                Left = 0
                Top = 0
                Width = 325
                Height = 302
                Margins.Left = 0
                Margins.Top = 0
                Margins.Right = 0
                Margins.Bottom = 0
                Align = alClient
                BorderStyle = bsNone
                DataSource = dsPeriod
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -16
                Font.Name = 'Tahoma'
                Font.Style = []
                Options = [dgTitles, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                ParentFont = False
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -19
                TitleFont.Name = 'Tahoma'
                TitleFont.Style = [fsBold]
              end
            end
            object Edit1: TEdit
              Left = 3
              Top = 108
              Width = 301
              Height = 27
              TabOrder = 1
              OnKeyPress = Edit1KeyPress
            end
          end
          object Panel7: TPanel
            AlignWithMargins = True
            Left = 712
            Top = 11
            Width = 541
            Height = 506
            Margins.Left = 10
            Margins.Top = 10
            Margins.Right = 10
            Margins.Bottom = 10
            Align = alRight
            TabOrder = 1
            object Shape1: TShape
              Left = 24
              Top = 22
              Width = 505
              Height = 385
              Brush.Color = clRed
              Brush.Style = bsFDiagonal
            end
            object Label5: TLabel
              Left = 168
              Top = 155
              Width = 225
              Height = 33
              Caption = 'Estat'#237'sticas do m'#234's'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -27
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object SpeedButton2: TSpeedButton
              Left = 392
              Top = 350
              Width = 123
              Height = 41
              Caption = 'Ver detalhes'
              OnClick = SpeedButton2Click
            end
          end
          object Panel1: TPanel
            AlignWithMargins = True
            Left = 11
            Top = 11
            Width = 330
            Height = 506
            Margins.Left = 10
            Margins.Top = 10
            Margins.Right = 10
            Margins.Bottom = 10
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 2
            object btnInsertCategory: TSpeedButton
              Left = 0
              Top = 0
              Width = 330
              Height = 56
              Align = alTop
              Caption = 'Inserir categoria'
              OnClick = btnInsertCategoryClick
              ExplicitTop = 8
            end
            object DBCtrlGrid1: TDBCtrlGrid
              AlignWithMargins = True
              Left = 10
              Top = 66
              Width = 310
              Height = 430
              Margins.Left = 10
              Margins.Top = 10
              Margins.Right = 10
              Margins.Bottom = 10
              Align = alClient
              DataSource = dsCategory
              PanelBorder = gbNone
              PanelHeight = 43
              PanelWidth = 293
              TabOrder = 0
              RowCount = 10
              OnPaintPanel = DBCtrlGrid1PaintPanel
              object Panel10: TPanel
                Left = 0
                Top = 0
                Width = 293
                Height = 43
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object lblCategory: TLabel
                  Left = 16
                  Top = 8
                  Width = 12
                  Height = 19
                  Caption = #8226' '
                end
              end
            end
          end
        end
      end
      object cardCategory: TCard
        Left = 0
        Top = 0
        Width = 1264
        Height = 528
        Caption = 'cardCategorias'
        CardIndex = 1
        TabOrder = 1
        object Panel11: TPanel
          Left = 0
          Top = 0
          Width = 1264
          Height = 528
          Align = alClient
          TabOrder = 0
        end
      end
    end
  end
  object pnlInsertPeriodForm: TPanel
    Left = 425
    Top = 202
    Width = 274
    Height = 150
    BevelInner = bvSpace
    BevelKind = bkTile
    TabOrder = 2
    Visible = False
    object Label6: TLabel
      Left = 45
      Top = 14
      Width = 180
      Height = 19
      Caption = 'Insira o per'#237'odo a seguir:'
    end
    object dpToday: TDatePicker
      AlignWithMargins = True
      Left = 32
      Top = 48
      Width = 204
      Date = 43428.000000000000000000
      DateFormat = 'MM/yyyy'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Segoe UI'
      Font.Style = []
      TabOrder = 0
    end
    object btnInsertPeriodForm: TButton
      Left = 32
      Top = 94
      Width = 100
      Height = 35
      Caption = 'Inserir'
      TabOrder = 1
      OnClick = btnInsertPeriodFormClick
    end
    object btnCancelPeriodForm: TButton
      Left = 138
      Top = 94
      Width = 100
      Height = 35
      Caption = 'Cancelar'
      TabOrder = 2
      OnClick = btnCancelPeriodFormClick
    end
  end
  object pnlInsertCategoryForm: TPanel
    Left = 74
    Top = 202
    Width = 274
    Height = 150
    BevelInner = bvSpace
    BevelKind = bkTile
    TabOrder = 3
    Visible = False
    object Label7: TLabel
      Left = 45
      Top = 14
      Width = 190
      Height = 19
      Caption = 'Insira o categoria a seguir:'
    end
    object btnInsertCategoryForm: TButton
      Left = 32
      Top = 94
      Width = 100
      Height = 35
      Caption = 'Inserir'
      TabOrder = 0
      OnClick = btnInsertCategoryFormClick
    end
    object btnCancelCategoryForm: TButton
      Left = 138
      Top = 94
      Width = 100
      Height = 35
      Caption = 'Cancelar'
      TabOrder = 1
      OnClick = btnCancelCategoryFormClick
    end
    object edtCategoryForm: TEdit
      Left = 34
      Top = 48
      Width = 201
      Height = 27
      TabOrder = 2
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 81
    Width = 1264
    Height = 72
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object sbtnSelectArea1: TSpeedButton
      Left = 14
      Top = 14
      Width = 159
      Height = 40
      GroupIndex = 2
      Down = True
      Caption = 'Per'#237'odo mensal'
      Flat = True
      OnClick = sbtnSelectArea1Click
    end
    object sbtnSelectArea2: TSpeedButton
      Left = 191
      Top = 14
      Width = 180
      Height = 40
      GroupIndex = 2
      Caption = 'Estat'#237'stica total anual'
      Flat = True
      OnClick = sbtnSelectArea2Click
    end
    object shpBtn1: TShape
      Left = 14
      Top = 58
      Width = 161
      Height = 9
      Brush.Color = clBlue
      Pen.Style = psClear
      Visible = False
    end
    object shpBtn2: TShape
      Left = 191
      Top = 58
      Width = 180
      Height = 9
      Brush.Color = clBlue
      Pen.Style = psClear
      Visible = False
    end
  end
  object dsPeriod: TDataSource
    DataSet = frmDM.qrPeriod
    Left = 367
    Top = 520
  end
  object dsCategory: TDataSource
    DataSet = frmDM.qrCategory
    Left = 16
    Top = 512
  end
end

unit untPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Samples.Spin,
  Vcl.Mask,
  Vcl.ComCtrls, Vcl.WinXPickers, Vcl.ExtCtrls, Vcl.WinXCtrls, Vcl.Buttons,
  Data.DB, Vcl.Grids, Vcl.DBGrids, Vcl.CategoryButtons, Vcl.ButtonGroup,
  Vcl.WinXPanels, Vcl.ValEdit, Vcl.DBCGrids;

type
  TfrmPrincipal = class(TForm)
    Panel3: TPanel;
    Panel5: TPanel;
    Panel4: TPanel;
    Panel6: TPanel;
    cpnActiveArea: TCardPanel;
    cardSpend: TCard;
    btnSearchMonth: TSpeedButton;
    btnSearchYear: TSpeedButton;
    Edit1: TEdit;
    Panel7: TPanel;
    Panel8: TPanel;
    dbGridPeriodo: TDBGrid;
    btnInsertPeriod: TSpeedButton;
    Shape1: TShape;
    Label5: TLabel;
    SpeedButton2: TSpeedButton;
    pnlInsertPeriodForm: TPanel;
    dpToday: TDatePicker;
    Label6: TLabel;
    btnInsertPeriodForm: TButton;
    dsPeriod: TDataSource;
    btnCancelPeriodForm: TButton;
    cardCategory: TCard;
    Panel9: TPanel;
    Panel1: TPanel;
    DBCtrlGrid1: TDBCtrlGrid;
    dsCategory: TDataSource;
    Panel10: TPanel;
    lblCategory: TLabel;
    pnlInsertCategoryForm: TPanel;
    Label7: TLabel;
    btnInsertCategoryForm: TButton;
    btnCancelCategoryForm: TButton;
    edtCategoryForm: TEdit;
    btnInsertCategory: TSpeedButton;
    sbtnSelectArea1: TSpeedButton;
    sbtnSelectArea2: TSpeedButton;
    shpBtn1: TShape;
    shpBtn2: TShape;
    Panel11: TPanel;
    procedure btnInsertPeriodClick(Sender: TObject);
    procedure btnInsertPeriodFormClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btnCancelPeriodFormClick(Sender: TObject);
    procedure DBCtrlGrid1PaintPanel(DBCtrlGrid: TDBCtrlGrid; Index: Integer);
    procedure btnInsertCategoryClick(Sender: TObject);
    procedure btnInsertCategoryFormClick(Sender: TObject);
    procedure btnCancelCategoryFormClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sbtnSelectArea1Click(Sender: TObject);
    procedure sbtnSelectArea2Click(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
    function ConvertLiteralMonth(valMonth : Integer) : String;
  public
    { Public declarations }
  end;

var
  frmPrincipal: TfrmPrincipal;

implementation

{$R *.dfm}

uses System.Generics.Collections, classe.MakeSpend, classe.MakeCategory,
     classe.MakePeriod, untDM, untTable;


function TfrmPrincipal.ConvertLiteralMonth(valMonth : Integer) : String;
var
  Dictionary : TDictionary<Integer, String>;
  LiteralMonth : String;
begin
  Dictionary := TDictionary<integer, String>.Create;
  Dictionary.Add(1, 'Janeiro');
  Dictionary.Add(2, 'Fevereiro');
  Dictionary.Add(3, 'Mar�o');
  Dictionary.Add(4, 'Abril');
  Dictionary.Add(5, 'Maio');
  Dictionary.Add(6, 'Junho');
  Dictionary.Add(7, 'Julho');
  Dictionary.Add(8, 'Agosto');
  Dictionary.Add(9, 'Setembro');
  Dictionary.Add(10, 'Outubro');
  Dictionary.Add(11, 'Novembro');
  Dictionary.Add(12, 'Dezembro');

  try
    Dictionary.TryGetValue(valMonth, LiteralMonth);
    Result := LiteralMonth;
  finally
    Dictionary.Free;
  end;

end;

procedure TfrmPrincipal.DBCtrlGrid1PaintPanel(DBCtrlGrid: TDBCtrlGrid; Index: Integer);
begin
  lblCategory.Caption := '� ' + untDM.frmDM.qrCategoryCATEGORY.Text;
end;

procedure TfrmPrincipal.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
  if btnSearchMonth.Down then
  begin
    // TODO:
    // acessa a classe makePeriod e passa os atributos:
    // makePeriod.SearchPeriod('month', 'janeiro')
  end
  else
  begin
    // acessa a classe makePeriod e passa os atributos:
    // makePeriod.SearchPeriod('year', '2018')
  end;

end;

procedure TfrmPrincipal.btnInsertPeriodFormClick(Sender: TObject);
var
  MyTime: TDateTime;
  MyPeriod: TMakePeriod;

  y, m, d: Word;
  month : String;

begin
  MyTime := dpToday.Date;
  DecodeDate(MyTime, y, m, d);

  MyPeriod := TMakePeriod.Create;
  try
    month := ConvertLiteralMonth(m);
    MyPeriod.createPeriod(month, y);

  finally
    MyPeriod.Free;

  end;

  pnlInsertPeriodForm.Visible := False;
end;



procedure TfrmPrincipal.btnInsertCategoryFormClick(Sender: TObject);
var
  MyCategory : TMakeCategory;

begin
  if edtCategoryForm.Text <> '' then
  begin
    MyCategory := TMakeCategory.Create;
    try
      MyCategory.createType(edtCategoryForm.Text);
    finally
      MyCategory.Free;
    end;
  end;

  pnlInsertCategoryForm.Visible := False;
end;



//
// *** BUTTONS ***
//
procedure TfrmPrincipal.btnCancelCategoryFormClick(Sender: TObject);
begin
  pnlInsertCategoryForm.Visible := False;
end;

procedure TfrmPrincipal.btnCancelPeriodFormClick(Sender: TObject);
begin
  pnlInsertPeriodForm.Visible := False;
end;

procedure TfrmPrincipal.btnInsertPeriodClick(Sender: TObject);
begin
  pnlInsertPeriodForm.Visible := True;
  dpToday.Date := Now;
end;

procedure TfrmPrincipal.btnInsertCategoryClick(Sender: TObject);
begin
  edtCategoryForm.Text := '';
  pnlInsertCategoryForm.Visible := True;
  edtCategoryForm.SetFocus;
end;

procedure TfrmPrincipal.sbtnSelectArea1Click(Sender: TObject);
begin
  if sbtnSelectArea1.Down then
  begin
    shpBtn1.Visible := True;
    cpnActiveArea.ActiveCard := cardSpend;
  end;

  shpBtn2.Visible := False;
end;

procedure TfrmPrincipal.sbtnSelectArea2Click(Sender: TObject);
begin
  if sbtnSelectArea2.Down then
  begin
    shpBtn2.Visible := True;
    cpnActiveArea.ActiveCard := cardCategory;
  end;

  shpBtn1.Visible := False;
end;

procedure TfrmPrincipal.SpeedButton2Click(Sender: TObject);
begin

  frmTableVisualizer := TfrmTableVisualizer.Create(Self);
  try
    frmTableVisualizer.Month := dbGridPeriodo.SelectedField.AsString;
    frmTableVisualizer.ShowModal;
  finally
    frmTableVisualizer.Free;
  end;
end;

//
// *** FORM EVENTS ***
//
procedure TfrmPrincipal.FormShow(Sender: TObject);
begin
  dbGridPeriodo.Columns[0].Width := 150;
  dbGridPeriodo.Columns[1].Width := 100;
  shpBtn1.Visible := True;
end;

procedure TfrmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  untDM.frmDM.qrPeriod.Close;
  untDM.frmDM.qrCategory.Close;
end;

procedure TfrmPrincipal.FormCreate(Sender: TObject);
begin
  untDM.frmDM.fdConn.Open;

  untDM.frmDM.qrCategory.Open;
  untDM.frmDM.qrPeriod.Open;
end;

end.

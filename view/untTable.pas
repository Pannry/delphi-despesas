unit untTable;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.Buttons;

type
  TfrmTableVisualizer = class(TForm)
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    Shape1: TShape;
    SpeedButton7: TSpeedButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FMonth : String;
    procedure SetMonth(const Value: String);
  public
    { Public declarations }
    property Month : String read FMonth write SetMonth;
  end;

var
  frmTableVisualizer: TfrmTableVisualizer;

implementation

{$R *.dfm}

uses untDM;

procedure TfrmTableVisualizer.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  untDM.frmDM.qrTableVisualizer.Close;
end;

procedure TfrmTableVisualizer.FormShow(Sender: TObject);
begin
  untDM.frmDM.qrTableVisualizer.ParamByName('SelectedMonth').AsString := Month;
  untDM.frmDM.qrTableVisualizer.Open;
end;

procedure TfrmTableVisualizer.SetMonth(const Value: String);
begin
  FMonth := Value;
end;

end.
